module.exports = {
  plugins: ['commitlint-plugin-jira-rules'],
  extends: ['jira'],
  rules: {
    'jira-task-id-max-length': [2, 'always', 10],
    'jira-task-id-min-length': [2, 'always', 4],
    'jira-task-id-project-key': [2, 'always', 'RB'],
    'jira-commit-message-separator': [2, 'always', ':'],
    'jira-task-id-separator': [2, 'always', '-'],
    'jira-task-id-case': [2, 'always', 'uppercase'],
  }
}