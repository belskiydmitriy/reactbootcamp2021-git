npm install --save-dev @commitlint/cli commitlint-plugin-jira-rules commitlint-config-jira
npm install husky --save-dev
npx husky add .husky/commit-msg 'commitlint --edit $1'

// commitlint.config.js
module.exports = {
plugins: ['commitlint-plugin-jira-rules'],
extends: ['jira'],
}

// package.json
"husky": {
"hooks": {
"commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
}
}

git init
git remote add origin https://gitlab.com/belskiydmitriy/reactbootcamp2021-git.git
git add .
git commit --amend -m "Bad commit message"

// Соглашения о коммитах

<type>(<scope>): <subject>

<BLANK LINE>

<body>

<BLANK LINE>

<footer>

<type>
build: изменения, влияющие на систему сборки или внешние зависимости, например: gulp, broccoli, npm
ci: изменения в наших файлах конфигурации и скриптах CI, например: Travis, Circle, BrowserStack, SauceLabs
docs: изменяется только документация
feat: новая функциональность
fix: исправление багов
perf: изменение кода, улучшающее производительность.
refactor: изменение кода, которое не исправляет ошибку и не добавляет функции
style: изменения, не влияющие на смысл кода (пробелы, форматирование, отсутствие точек с запятой и т. д.)
test: добавление недостающих тестов или исправление существующих тестов

// примеры
docs: remove devDependencies badge from README (#29499)
chore: bump chromium to 93.0.4530.0 (master) (#29256)
refactor: use auto env = base::Environment::Create(); everywhere (#29502 …
fix: change ASAR archive cache to per-process to fix leak (#29293) …
build: remove individual release build workflows (#29344) …
feat: add new imageAnimationPolicy webpref and webContents setter (#2… …
ci: save artifacts before cleaning up big things on macOS (#29578)
chore(release): 11.1.0
chore(dependencies): update 21/03/2021
fix(eslint): update to new eslint-config-prettier usage
feat(babel+ts): babel process will now resolve aliases in d.ts files … …
chore(ci): login to ghcr too …
chore(deps): bump github.com/golangci/golangci-lint (#289) …
test: Add APK and RSA sign test (#233) …
